import sys
import logging
import os
import re

from packaging import version

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

root_dir = os.path.dirname(__file__)

versions = {}

class VersionError(Exception):
    pass


f = open(os.path.join(root_dir, 'requirements.txt'), 'r')
while True:
    l = f.readline().rstrip()
    if l == '':
        break
    (m, v) = l.split('==')
    logg.debug('read {} => {}'.format(m, v))
    versions[m.lower()] = version.parse(v)
f.close()


re_versionline = r'^(.+)([=\~\>\<]=)(\d.+)$'
f = open(sys.argv[1], 'r')
while True:
    l = f.readline().rstrip()
    if l == '':
        break

    match = re.match(re_versionline, l)
    logg.debug('checking line {}; {} {} {}'.format(l, match[1], match[2], match[3]))

    modulename = match[1].replace('_', '-').lower()

    if modulename[:4] == 'cic-':
        logg.info('skipping cic internal version {} => {}'.format(modulename, match[3]))
        continue

    shouldhave = versions[modulename]
    have = version.parse(match[3])

    fault = None
    
    if match[2] == '~=':
        logg.debug('checking COMPATIBLE match for {} => {}'.format(modulename, match[3]))
        if shouldhave.major > 0 and have.major < shouldhave.major:
            fault = 'COMPATIBLE WiTH'
        elif shouldhave.major == 0 and have.minor < shouldhave.minor:
            fault = 'COMPATIBLE WiTH'
        elif shouldhave.major == 0 and shouldhave.minor == 0:
            if have.is_prerelease and have < shouldhave:
                fault = 'COMPATIBLE WITH'
            elif have.micro < shouldhave.micro:
                fault = 'COMPATIBLE WiTH'

    elif match[2] == '==':
        logg.debug('checking EXACT match for {} => {}'.format(modulename, match[3]))
        if not shouldhave == have:
            fault = 'EXACT MATCH OF'

    elif match[2] == '>':
        logg.debug('checking GREATED THAN match for {} => {}'.format(modulename, match[3]))
        if not shouldhave < have:
            fault = 'GREATER THAN'

    elif match[2] == '>=':
        logg.debug('checking GREATED THAN OR EQUAL match for {} => {}'.format(modulename, match[3]))
        if not shouldhave <= have:
            fault = 'GREATER OR EQUAL THAN'

    elif match[2] == '<':
        logg.debug('checking LESSER THAN match for {} => {}'.format(modulename, match[3]))
        if not shouldhave < have:
            fault = 'LESSER THAN'

    elif match[2] == '<=':
        logg.debug('checking LESSER THAN OR EQUAL match for {} => {}'.format(modulename, match[3]))
        if not shouldhave <= have:
            fault = 'LESSER OR EQUAL THAN'
    else:
        raise ValueError('caught comparison modifier {}, dunno what that is'.format(match[2]))

    if fault != None:
        raise VersionError('{}: {} not {} {}'.format(modulename, have, fault, shouldhave))
